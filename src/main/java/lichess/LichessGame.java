package lichess;

/**
 * @author Drew Pleat
 * @version 1.2 03/22/20
 * A class that stores the data in a lichess game, mainly used for returning lots of data at once from a function.
 * @since 1.2
 */
public class LichessGame {
	public final String p1, p2;
	public final long timestamp;
	public final String modeStr;
	public final String winner;
	public final String id;

	LichessGame(String p1, String p2, long timestamp, String modeStr, String winner, String id) {
		this.p1 = p1;
		this.p2 = p2;

		this.timestamp = timestamp;
		this.modeStr = modeStr;
		this.winner = winner;
		this.id = id;
	}
}
