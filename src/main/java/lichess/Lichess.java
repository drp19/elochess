package lichess;

import com.google.gson.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author Drew Pleat
 * @version 1.2 03/22/20
 * Static methods for accessing the lichess API. Currently checks accounts and downloads games.
 * @since 1.2
 */
public final class Lichess {
	/**
	 * Checks to see if a username is a valid account on Lichess
	 *
	 * @param userName a lichess username
	 * @return True if the account is valid, false if it is not or the request fails otherwise.
	 */
	public static boolean checkAccount(String userName) {
		try {
			JsonElement object = readJsonFromUrl("https://lichess.org/api/users/status?ids=" + userName);
			JsonArray arr = object.getAsJsonArray();
			return arr.size() != 0;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Gets data from a game on Lichess
	 *
	 * @param urlStr The URL of the game. Takes the URL path, shortens, then uses the API.
	 * @return Returns null if the game could not be parsed.
	 */
	public static LichessGame addGame(String urlStr) {
		try {
			URL url = new URL(urlStr);
			//make sure game id is short enough
			String gameID = url.getPath().substring(1, 9);
			JsonObject object = readJsonFromUrl("https://lichess.org/game/export/" + gameID).getAsJsonObject();
			//process status
			String status = object.get("status").getAsString();
			String winner;
			if (status.equals("draw")) {
				winner = "DRAW";
			} else if (status.equals("mate") || status.equals("resign")) {
				winner = object.get("winner").getAsString();
			} else {
				return null;
			}
			//process time, players
			long time = object.get("lastMoveAt").getAsLong();
			JsonObject players = object.getAsJsonObject("players");
			String p1 = players.getAsJsonObject("white").getAsJsonObject("user").get("id").getAsString();
			String p2 = players.getAsJsonObject("black").getAsJsonObject("user").get("id").getAsString();
			if (!winner.equals("DRAW")) {
				winner = (winner.equals("white")) ? p1 : p2;
			}
			//process mode
			String mode;
			JsonObject clock = object.getAsJsonObject("clock");
			if (clock == null) {
				mode = "untimed";
			} else {
				int start = clock.get("initial").getAsInt(), inc = clock.get("increment").getAsInt();
				mode = (start / 60) + "+" + inc;
			}
			return new LichessGame(p1, p2, time, mode, winner, gameID);
		} catch (JsonParseException e) {
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * FROM https://www.baeldung.com/java-http-request
	 * Method for actually getting data from the API.
	 *
	 * @param urlStr The URL to request from, parameters included.
	 * @return Returns a JSON response from the website.
	 * @throws IOException Occurs when the data is not JSON or the request fails otherwise.
	 */
	private static JsonElement readJsonFromUrl(String urlStr) throws IOException {
		URL url = new URL(urlStr);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Accept", "application/json");
		BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
		JsonElement json = new Gson().fromJson(rd, JsonElement.class);
		rd.close();
		con.disconnect();
		return json;
	}
}
