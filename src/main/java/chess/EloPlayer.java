package chess;

import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static chess.EloMain.DEFAULT;

/**
 * @author Drew Pleat
 * @version 1.2 03/22/20
 * Represents a player in the ELO system. Stores win record and ELO for each mode, and has methods to:
 * - Update and print record
 * - Update, reset, get, and print data for a given mode
 * Implements cloning for use in saving ELO data over time.
 */
public class EloPlayer implements Cloneable {
	private transient HashMap<EloMode, ModePlayer> modes;
	final String name;
	String lichess;

	public EloPlayer(@NotNull String name) {
		this.name = name;
		modes = new HashMap<>();
	}

	public float getRating(EloMode mode) {
		return getMP(mode).elo;
	}

	/**
	 * Increment the wins of a certain mode by 1
	 *
	 * @param mode the mode to add a win to
	 */
	public void win(EloMode mode) {
		getMP(mode).win++;
	}

	/**
	 * Increment the losses of a certain mode by 1
	 *
	 * @param mode the mode to add a loss to
	 */
	public void loss(EloMode mode) {
		getMP(mode).loss++;
	}

	/**
	 * Increment the draws of a certain mode by 1
	 *
	 * @param mode the mode to add a draw to
	 */
	public void draw(EloMode mode) {
		getMP(mode).draw++;
	}

	/**
	 * Creates a win-loss-draw string for the user
	 *
	 * @param mode the mode to get stats for
	 * @return A string with win,loss,draw stats
	 */
	public String getRecord(EloMode mode) {
		ModePlayer mp = getMP(mode);
		return "Record: " + mp.win + "-" + mp.loss + "-" + mp.draw;
	}

	/**
	 * Update the ELO for a player by a differential.
	 *
	 * @param mode The mode to update.
	 * @param diff the differential to change by.
	 */
	public void updateElo(EloMode mode, float diff) {
		ModePlayer mp = getMP(mode);
		mp.elo = mp.elo + diff;
	}

	/**
	 * Sets ELO for a mode to {@link EloMain#DEFAULT}
	 *
	 * @param mode The mode to reset.
	 */
	public void resetElo(EloMode mode) {
		getMP(mode).elo = DEFAULT;
	}

	/**
	 * Prints a formatted output of the name and ELO.
	 *
	 * @param mode The mode to print for (most likely is a valid mode, but doesn't have to be)
	 * @return String to print.
	 */
	public String printElo(EloMode mode) {
		float ret = getRating(mode);
		return name + " - " + Math.round(ret);
	}

	/**
	 * Gets the {@link ModePlayer} for the specified mode, or creates a new one.
	 *
	 * @param mode A mode, can be null.
	 * @return A new, or found object.
	 */
	@NotNull
	private ModePlayer getMP(EloMode mode) {
		ModePlayer mp = modes.get(mode);
		if (mp == null) {
			mp = new ModePlayer();
			modes.put(mode, mp);
		}
		return mp;
	}

	/**
	 * Clones the object, deep copying each ModePlayer so that it saves the statistics at that time.
	 * when the game happens.
	 *
	 * @return The cloned object
	 */
	@Override
	protected EloPlayer clone() {
		try {
			EloPlayer ret = (EloPlayer) super.clone();
			ret.modes = new HashMap<>();
			//deep copy ModePlayers
			for (Map.Entry<EloMode, ModePlayer> eloEntry : modes.entrySet()) {
				ret.modes.put(eloEntry.getKey(), eloEntry.getValue().clone());
			}
			return ret;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e); //should never happen.
		}
	}

	/**
	 * Checks that the names are the same - two players with the same name cannot exist
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EloPlayer eloPlayer = (EloPlayer) o;
		return name.equals(eloPlayer.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	/**
	 * Any print operation should come through {@link #printElo(EloMode)}
	 */
	@Override
	public String toString() throws UnsupportedOperationException {
		throw new UnsupportedOperationException("EloPlayer should not be printed.");
	}

	/**
	 * An object presenting a player's statistics in a specific mode.
	 */
	private static class ModePlayer implements Cloneable {
		private float elo;
		private int win, loss, draw;

		private ModePlayer() {
			elo = DEFAULT;
			win = loss = draw = 0;
		}

		@Override
		protected ModePlayer clone() {
			try {
				return (ModePlayer) super.clone();
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e); //should never happen
			}
		}
	}
}

/**
 * Sorts players by their ELO in a specific mode.
 */
class EloComparator implements Comparator<EloPlayer> {
	private EloMode mode;

	public EloComparator(EloMode mode) {
		this.mode = mode;
	}

	@Override
	public int compare(@NotNull EloPlayer p1, @NotNull EloPlayer p2) {
		return Float.compare(p2.getRating(mode), p1.getRating(mode));
	}
}
