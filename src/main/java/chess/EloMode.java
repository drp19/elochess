package chess;

import java.util.Objects;

/**
 * @author Drew Pleat
 * @version 1.1 03/21/20
 * Represents a game mode. Currently is basically an abstraction of String, but may do more in the future.
 */
public class EloMode {
	private final String name;

	public EloMode(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EloMode eloMode = (EloMode) o;
		return name.equals(eloMode.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}
}
