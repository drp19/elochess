package chess;

import chess.EloGame.Winner;
import com.google.gson.*;
import lichess.Lichess;
import lichess.LichessGame;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author Drew Pleat
 * @version 1.2, 03/22/20
 * <p>
 * An object representing the ELO ranking system. Contains methods to:
 * - play, delete, and list games.
 * - add and list modes.
 * - add and list players.
 * Most operations are extremely efficient, with the exceptions of listing players and deleting a game, which require additional computation.
 * Includes a GSON deserializer in order to calculate the ELO values.
 * Because no ELO data is stored permanently, the formula (and K value can change as needed)
 */
public class EloMain {

	static final int DEFAULT = 1200;

	private static Logger logger = LoggerFactory.getLogger(EloMain.class);
	private EloMode DEFAULT_MODE;
	private String serverID;
	private Map<String, EloPlayer> players;
	private Map<String, EloGame> games; //for finding, removing which require
	private transient TreeSet<EloGame> orderedGames; //for printing, initializing which require sorted data
	private Map<String, EloMode> modes;

	/**
	 * Creates a new ELO ranking system. Saves the serverID for ease, but has no relevance.
	 *
	 * @param serverID The server ID.
	 */
	public EloMain(@NotNull String serverID) {
		this.serverID = serverID;
		this.players = new HashMap<>();
		this.games = new HashMap<>();
		this.orderedGames = new TreeSet<>(Comparator.comparing(g -> g.timestamp));
		this.modes = new HashMap<>();
	}

	/**
	 * Creates a random string of uppercase alphanumeric characters.
	 * FROM https://www.baeldung.com/java-random-string
	 *
	 * @return the string of characters.
	 */
	@NotNull
	private static String genID() {
		int leftLimit = 65; // letter 'A'
		int rightLimit = 90; // letter 'Z'
		int targetStringLength = 4; //TODO: Make this a variable somewhere.
		Random random = new Random();
		StringBuilder buffer = new StringBuilder(targetStringLength);
		for (int i = 0; i < targetStringLength; i++) {
			int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
			buffer.append((char) randomLimitedInt);
		}
		return buffer.toString();
	}

	/**
	 * Lists the games in order of occurance, filtered by mode or player.
	 *
	 * @param modeStr   Optional parameter, filters to a specific mode. Does not apply filter if invalid mode.
	 * @param playerStr Optional parameter, filters to a specific player. Does not apply filter if invalid player name.
	 * @param toPrint   Object to output list to, if successful. CLEARS ANY DATA IN THE OBJECT.
	 * @return Returns {@link EloResult#NONE} if no results are found, {@link EloResult#SUCCESS} otherwise.
	 */
	public EloResult listGames(@Nullable String modeStr, @Nullable String playerStr, @NotNull StringBuffer toPrint) {
		logger.info(serverID + " games<" + modeStr + "><" + playerStr + ">");
		toPrint.setLength(0);
		//validate mode
		boolean allMode = modeStr == null;
		EloMode mode = modes.get(modeStr);
		allMode = allMode || mode == null;
		//validate player
		boolean allPlayer = playerStr == null;
		EloPlayer player = players.get(playerStr);
		allPlayer = allPlayer || player == null;
		//filter data, separate into modes for printing
		boolean finalAllMode = allMode, finalAllPlayer = allPlayer;
		Predicate<EloGame> filterMode = g -> finalAllMode || g.mode == mode;
		Predicate<EloGame> filterPlayer = g -> finalAllPlayer || g.p1 == player || g.p2 == player;
		Map<EloMode, List<EloGame>> printGames = orderedGames.stream().filter(filterPlayer).filter(filterMode).collect(Collectors.groupingBy(EloGame::getMode));
		//print
		for (Map.Entry<EloMode, List<EloGame>> modeEntry : printGames.entrySet()) {
			toPrint.append(modeEntry.getKey()).append("\n");
			for (EloGame g : modeEntry.getValue()) {
				toPrint.append(g).append("\n");
			}
		}
		if (printGames.size() == 0) {
			return EloResult.NONE;
		}
		if (!allPlayer) {
			toPrint.append(player.getRecord(mode)).append("\n");
		}
		return EloResult.SUCCESS;
	}

	/**
	 * Lists the modes that have been added.
	 *
	 * @param data Object that the data wil be outputted to. CLEARS ALL EXISTING DATA.
	 * @return Returns {@link EloResult#NONE} if no results are found, {@link EloResult#SUCCESS} otherwise.
	 */
	public EloResult listModes(@NotNull StringBuffer data) {
		logger.info(serverID + " mode");
		data.setLength(0);
		if (modes.size() == 0) {
			return EloResult.NONE;
		}
		for (EloMode m : modes.values()) {
			data.append(m).append("\n");
		}
		return EloResult.SUCCESS;
	}

	/**
	 * Adds a new mode to the list of modes.
	 *
	 * @param mode The name of the mode. CASE SENSITIVE!
	 * @return {@link EloResult#SUCCESS} if the mode has a valid name without spaces and is not already present, otherwise {@link EloResult#INVALID_PARAM_ADD}
	 */
	public EloResult addMode(String mode) {
		logger.info(serverID + " mode<" + mode + ">");
		mode = mode.replaceAll("\\s+", ""); //remove spaces
		if (mode.length() == 0) {
			return EloResult.INVALID_PARAM_ADD;
		}
		EloMode modeObj = new EloMode(mode);
		if (modes.size() == 0) {
			DEFAULT_MODE = modeObj;
		}
		if (modes.putIfAbsent(mode.toLowerCase(), modeObj) != null) {
			return EloResult.INVALID_PARAM_ADD;
		}
		return EloResult.SUCCESS;
	}

	/**
	 * Returns the standings of players who have played a game in the specified mode.
	 *
	 * @param modeStr Optional parameter to specify mode to list standings for, otherwise default mode. Returns {@link EloResult#INVALID_MODE} if invalid mode.
	 * @param data    The object to output data to. CLEARS ALL EXISTING DATA.
	 * @return @return Returns {@link EloResult#NONE} if no results are found, {@link EloResult#SUCCESS} otherwise.
	 */
	public EloResult listPlayers(@Nullable String modeStr, @NotNull StringBuffer data) {
		logger.info(serverID + " players<" + modeStr + ">");
		data.setLength(0);
		//validate mode
		EloMode mode;
		if (modeStr == null) {
			mode = DEFAULT_MODE;
		} else {
			mode = modes.get(modeStr);
		}
		if (mode == null) {
			return EloResult.INVALID_MODE;
		}
		ArrayList<EloPlayer> list = new ArrayList<>(players.values());
		boolean empty = true;
		list.sort(new EloComparator(mode));
		for (EloPlayer p : list) {
			if (p.getRating(mode) != DEFAULT) {
				data.append(p.printElo(mode)).append("\n");
				empty = false;
			}
		}
		if (empty) {
			return EloResult.NONE;
		}
		return EloResult.SUCCESS;
	}

	/**
	 * Deletes the game from the data. Due to the nature of the data, it must then recalculate the ELO of all games.
	 *
	 * @param game The game ID to be deleted. CASE SENSITIVE!
	 * @return Returns {@link EloResult#INVALID_NUMBER} if the game does not exist, otherwise {@link EloResult#SUCCESS}
	 */
	public EloResult deleteGame(@NotNull String game) {
		logger.info(serverID + " delete<" + game + ">");
		EloGame del = games.remove(game);
		if (del == null) {
			return EloResult.INVALID_NUMBER;
		}
		orderedGames.remove(del);
		//reset all ELO for that given mode.
		for (EloPlayer p : players.values()) {
			p.resetElo(del.mode);
		}
		reCalc(del.mode);
		return EloResult.SUCCESS;
	}

	/**
	 * Adds a player to the game data
	 *
	 * @param name Name of mode. CASE SENSITIVE!
	 * @return {@link EloResult#SUCCESS} if the player has a valid name without spaces and is not already present, otherwise {@link EloResult#INVALID_PARAM_ADD}
	 */
	public EloResult addPlayer(@NotNull String name) {
		logger.info(serverID + " add<" + name + ">");
		name = name.replaceAll("\\s+", ""); //remove all spaces
		if (name.length() == 0) {
			return EloResult.INVALID_PARAM_ADD;
		}
		if (players.putIfAbsent(name.toLowerCase(), new EloPlayer(name)) != null) {
			return EloResult.INVALID_PARAM_ADD;
		}
		return EloResult.SUCCESS;
	}

	/**
	 * Gets a Lichess game and stores it with all the data a normal game would have. Orders in time.
	 *
	 * @param url URL of the game
	 * @return Returns {@link EloResult#INVALID_PARAM_URL} if the request fails, {@link EloResult#INVALID_PLAYER} if either player is not linked,
	 * otherwise returns the result of {@link #playGame(String, String, String, String, Long, String)}
	 */
	public EloResult playLichess(@NotNull String url) {
		logger.info(serverID + " play<" + url + ">");
		LichessGame game = Lichess.addGame(url);
		if (game == null) {
			return EloResult.INVALID_PARAM_URL;
		}
		addMode(game.modeStr);
		Optional<EloPlayer> p1 = players.values().stream().filter(p -> p.lichess != null && p.lichess.equals(game.p1)).findFirst();
		Optional<EloPlayer> p2 = players.values().stream().filter(p -> p.lichess != null && p.lichess.equals(game.p2)).findFirst();
		String p1N, p2N, winner;
		if (p1.isPresent() && p2.isPresent()) {
			p1N = p1.get().name;
			p2N = p2.get().name;
			if (game.winner.equals("DRAW")) {
				winner = "DRAW";
			} else if (game.winner.equals(p1.get().lichess)) {
				winner = p1N;
			} else {
				winner = p2N;
			}
		} else {
			return EloResult.INVALID_PLAYER;
		}
		return playGame(game.modeStr, p1N, p2N, winner, game.timestamp, game.id);
	}

	/**
	 * Implements the actual playing of the game used in the public method and in the lichess integration.
	 *
	 * @param timestamp For lichess.
	 * @param id        For lichess.
	 * @see #playGame(String, String, String, String)
	 */
	private EloResult playGame(@Nullable String modeStr, @NotNull String p1N, @NotNull String p2N, @NotNull String winner, @NotNull Long timestamp, @NotNull String id) {
		logger.info(serverID + " play<" + p1N + "><" + p2N + ">");
		//VALIDATE PLAYER,MODE
		EloPlayer p1 = players.get(p1N.toLowerCase()), p2 = players.get(p2N.toLowerCase());
		EloMode mode;
		if (modeStr == null) {
			mode = DEFAULT_MODE;
		} else {
			mode = modes.get(modeStr.toLowerCase());
		}
		if (mode == null) {
			return EloResult.INVALID_MODE;
		}
		if (p1 == null || p2 == null) {
			return EloResult.INVALID_PLAYER;
		}
		//VALIDATE WINNER
		Winner won;
		if (winner.equalsIgnoreCase(p1N)) {
			won = Winner.P1WIN;
		} else if (winner.equalsIgnoreCase(p2N)) {
			won = Winner.P2WIN;
		} else if (winner.equalsIgnoreCase("DRAW")) {
			won = Winner.DRAW;
		} else {
			return EloResult.INVALID_WINNER;
		}
		EloGame game = new EloGame(mode, p1, p2, won, id, timestamp);
		game.calcRating();
		games.put(id, game);
		orderedGames.add(game);
		if (orderedGames.last() != game) {
			reCalc(mode);
		}
		return EloResult.SUCCESS;
	}

	/**
	 * Attempts to add a game to the dataset. All parameters case insensitive. Creates a new ID and gets the current time.
	 *
	 * @param modeStr Optional parameter for the game's mode. Uses default mode if no mode is selected. Returns {@link EloResult#INVALID_MODE} if the mode name is not present.
	 * @param p1N     The game's first player. Returns {@link EloResult#INVALID_PLAYER} if it does not exist.
	 * @param p2N     The game's second player.
	 * @param winner  String representing the winner. Should be the name of the player, or 'DRAW'. Returns {@link EloResult#INVALID_WINNER} if it is not those.
	 * @return Returns success if none of the parameters are incorrect.
	 */
	public EloResult playGame(@Nullable String modeStr, @NotNull String p1N, @NotNull String p2N, @NotNull String winner) {
		String id;
		do {
			id = genID();
		} while (games.containsKey(id));
		long time = System.currentTimeMillis();
		return playGame(modeStr, p1N, p2N, winner, time, id);
	}

	/**
	 * Links a player with a lichess account, checks that the user exists.
	 *
	 * @param name The player name to link. Case insensitive.
	 * @param user The lichess user to link
	 * @return Returns {@link EloResult#INVALID_PARAM_URL} if invalid username or the request failed,
	 * {@link EloResult#INVALID_PARAM_ADD} if the player doesn't exist, {@link EloResult#SUCCESS} otherwise.
	 */
	public EloResult linkLichess(@NotNull String name, @NotNull String user) {
		logger.info(serverID + " link<" + name + "><" + user + ">");
		if (Lichess.checkAccount(user)) {
			EloPlayer p = players.get(name.toLowerCase());
			if (p == null) {
				return EloResult.INVALID_PLAYER;
			}
			if (p.lichess == null) {
				p.lichess = user;
			} else {
				return EloResult.INVALID_PARAM_ADD;
			}
		} else {
			return EloResult.INVALID_PARAM_URL;
		}
		return EloResult.SUCCESS;
	}

	/**
	 * Returns the default mode
	 */
	public EloMode getDefault() {
		return DEFAULT_MODE;
	}

	/**
	 * Recalculate ELO in order for a given mode.
	 */
	private void reCalc(EloMode mode) {
		for (EloGame g : orderedGames) {
			if (g.mode == mode) {
				g.calcRating();
			}
		}
	}

	/**
	 * Allows for loading the data from a file: required because most data is stored differently than default.
	 */
	public static class EloMainDeserializer implements JsonDeserializer<EloMain> {
		@Override
		public EloMain deserialize(@NotNull JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			JsonObject obj = json.getAsJsonObject();
			String serverID = obj.getAsJsonPrimitive("serverID").getAsString();
			logger.info(serverID + " load");
			EloMain chess = new EloMain(serverID);
			//add players
			for (JsonElement entry : obj.getAsJsonArray("players")) {
				String name = entry.getAsJsonObject().get("name").getAsString();
				chess.addPlayer(name);
				if (entry.getAsJsonObject().has("lichess")) {
					chess.players.get(name.toLowerCase()).lichess = entry.getAsJsonObject().get("lichess").getAsString();
				}
			}
			//add modes
			for (JsonElement entry : obj.getAsJsonArray("modes")) {
				chess.addMode(entry.getAsJsonObject().get("name").getAsString());
			}
			String def = obj.getAsJsonObject("DEFAULT_MODE").getAsJsonPrimitive("name").getAsString();
			chess.DEFAULT_MODE = def == null ? null : chess.modes.get(def);
			//add games
			for (JsonElement entry : obj.getAsJsonArray("games")) {
				JsonObject gameObj = entry.getAsJsonObject();
				String id = gameObj.get("id").getAsString();
				String modeStr = gameObj.get("mode").getAsString();
				EloMode mode = chess.modes.get(modeStr.toLowerCase());
				String p1str = gameObj.get("p1").getAsString();
				String p2str = gameObj.get("p2").getAsString();
				EloPlayer p1 = chess.players.get(p1str.toLowerCase()), p2 = chess.players.get(p2str.toLowerCase());
				long time = gameObj.get("timestamp").getAsLong();
				Winner won = null;
				switch (gameObj.get("winner").getAsString()) {
					case "p1":
						won = Winner.P1WIN;
						break;
					case "p2":
						won = Winner.P2WIN;
						break;
					case "dr":
						won = Winner.DRAW;
						break;
				}
				chess.orderedGames.add(new EloGame(mode, p1, p2, won, id, time));
			}
			//calculate ELO for each game
			for (EloGame g : chess.orderedGames) {
				g.calcRating();
				chess.games.put(g.id, g);
			}
			return chess;
		}

	}
}