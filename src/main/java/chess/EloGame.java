package chess;

import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * @author Drew Pleat
 * @version 1.1 03/21/20
 * Represents a game in the ELO system. Stores the players, the mode, and who won. Not much more to it than that.
 */
public class EloGame {
	private static final int K = 15;
	final EloPlayer p1;
	final EloPlayer p2;
	final String id;
	final EloMode mode;
	final long timestamp;
	final Winner winner;
	private transient EloPlayer p1C, p2C;

	enum Winner {
		@SerializedName("p1") P1WIN,
		@SerializedName("p2") P2WIN,
		@SerializedName("dr") DRAW
	}

	/**
	 * Creates a game object.
	 *
	 * @param mode      The mode of the game.
	 * @param p1        The first player.
	 * @param p2        The second player.
	 * @param win       The winner.
	 * @param id        the ID that the game is stored with. Used for printing and hashing.
	 * @param timestamp the epoch time of the game. Used for ordering chronologically.
	 */
	public EloGame(@NotNull EloMode mode, @NotNull EloPlayer p1, @NotNull EloPlayer p2, @NotNull Winner win, @NotNull String id, long timestamp) {
		winner = win;
		this.id = id;
		this.mode = mode;
		this.timestamp = timestamp;
		this.p1 = p1;
		this.p2 = p2;
	}

	/**
	 * Part of the ELO calculation. NOTICE: THIS IS NOT MINE! NOT SURE WHERE I GOT IT
	 */
	private static float Probability(float rating1, float rating2) {
		return 1.0f * 1.0f / (1 + 1.0f * (float) (Math.pow(10, 1.0f * (rating1 - rating2) / 400)));
	}

	/**
	 * Required for stream processing, even though it isn't a private field.
	 */
	public EloMode getMode() {
		return mode;
	}

	/**
	 * Updates the ratings for the players of the game based on the result. Clones the player objects for printing later,
	 * then changes player records and elo.
	 */
	public void calcRating() {
		p1C = p1.clone();
		p2C = p2.clone();
		float Pb = Probability(p1.getRating(mode), p2.getRating(mode));
		float Pa = Probability(p2.getRating(mode), p1.getRating(mode));
		float p1Diff = 0, p2Diff = 0;
		switch (winner) {
			case DRAW:
				p1Diff = K * (.5f - Pa);
				p1.draw(mode);
				p2Diff = K * (.5f - Pb);
				p2.draw(mode);
				break;
			case P1WIN:
				p1Diff = K * (1 - Pa);
				p1.win(mode);
				p2Diff = K * (0 - Pb);
				p2.loss(mode);
				break;
			case P2WIN:
				p1Diff = K * (0 - Pa);
				p1.loss(mode);
				p2Diff = K * (1 - Pb);
				p2.win(mode);
				break;
		}
		p1.updateElo(mode, p1Diff);
		p2.updateElo(mode, p2Diff);
	}

	@Override
	public String toString() {
		switch (winner) {
			case P1WIN:
				return id + " -> Winner: " + p1C.printElo(mode) + "  Loser: " + p2C.printElo(mode);
			case P2WIN:
				return id + "-> Winner: " + p2C.printElo(mode) + "  Loser: " + p1C.printElo(mode);
			case DRAW:
				return id + "-> DRAW: " + p1C.printElo(mode) + "   " + p2C.printElo(mode);
		}
		return null;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EloGame eloGame = (EloGame) o;
		return id.equals(eloGame.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}


