package chess;

public enum EloResult {
    SUCCESS("Success."),NONE("No results."),
    INVALID_MODE("Invalid mode. See /chess mode"),
    INVALID_PARAM_ADD("Invalid parameter. Check that it doesn't already exist, and has non-whitespace characters."),
    INVALID_PLAYER("Invalid player name. See /chess top"),
    INVALID_PARAMS("Invalid numbers of parameters. Check /chess help"),
    INVALID_WINNER("Winner is not one of the two players or \"DRAW\""),
    INVALID_NUMBER("Invalid number parameter. Check /chess games"),
    INVALID_PARAM_URL("The request failed. Make sure your URL is correct."),
    ERR("Internal error. The command may or may not have succeeded. Please contact me.");

    public final String toPrint;
    EloResult(String s) {
        toPrint = s;
    }
}
