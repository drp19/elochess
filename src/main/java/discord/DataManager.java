package discord;

import chess.EloGame;
import chess.EloMain;
import chess.EloMode;
import com.google.gson.*;

import java.io.*;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * @author Drew Pleat
 * @version 1.1 03/21/20
 * Implements the storing and loading of JSON files through the GSON library. All GSON functions are here except for
 * the one in EloMain that deals with private fields.
 */
public class DataManager {
	private static GsonBuilder gsonBuilder = new GsonBuilder();

	static {
		gsonBuilder.registerTypeAdapter(EloMain.class, new EloMain.EloMainDeserializer());
		gsonBuilder.registerTypeAdapter(EloGame.class, new EloGameSerializer());
		gsonBuilder.registerTypeAdapter(Map.class, new MapSerializer());
		gsonBuilder.setPrettyPrinting();
	}

	/**
	 * Loads the game from a file, or creates a new object if the file doesn't exist.
	 * @param file The file location
	 * @return A generated ELO game object.
	 */
	public static EloMain load(String file) {
		FileReader jsonIn;
		try {
			jsonIn = new FileReader(new File(file));
		} catch (FileNotFoundException e) {
			return new EloMain(file);
		}
		return gsonBuilder.create().fromJson(jsonIn, EloMain.class);
	}

	/**
	 * Saves the object to a file
	 * @param chess The object to save.
	 * @param out The file location to write.
	 * @throws IOException Can happen, shouldn't though.
	 */
	public static void save(EloMain chess, String out) throws IOException {
		String json = gsonBuilder.create().toJson(chess);
		FileWriter writer = new FileWriter(new File(out));
		writer.write(json);
		writer.close();
	}

	/**
	 * Saves a game with primative fields, since they point to other data structures saved fully.
	 */
	static class EloGameSerializer implements JsonSerializer<EloGame> {
		@Override
		public JsonElement serialize(EloGame src, Type typeOfSrc, JsonSerializationContext context) {
			GsonBuilder builder = new GsonBuilder();
			builder.registerTypeAdapter(EloMode.class, new EloModeSerializer());
			JsonObject obj = builder.create().toJsonTree(src).getAsJsonObject();
			obj.addProperty("p1",obj.getAsJsonObject("p1").get("name").getAsString());
			obj.addProperty("p2",obj.getAsJsonObject("p2").get("name").getAsString());
			return obj;
		}

		static class EloModeSerializer implements JsonSerializer<EloMode> {
			@Override
			public JsonElement serialize(EloMode src, Type typeOfSrc, JsonSerializationContext context) {
				return new JsonPrimitive(src.getName());
			}
		}
	}

	/**
	 * Since maps are indexed by ID, and ID is already stored, treat them as arrays.
	 */
	static class MapSerializer implements JsonSerializer<Map> {
		@Override
		public JsonElement serialize(Map src, Type typeOfSrc, JsonSerializationContext context) {
			JsonArray arr = new JsonArray();
			for (Object b : src.values()) {
				arr.add(context.serialize(b));
			}
			return arr;
		}
	}
}
