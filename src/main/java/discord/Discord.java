package discord;

import chess.EloMain;
import chess.EloResult;
import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.domain.guild.GuildCreateEvent;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Drew Pleat
 * @version 1.2 03/22/20
 * Contains all the abstraction for discord. Each server has a channel mapped to it where commands work.
 */
public class Discord {
	private static final Map<String, Command> commands = new HashMap<>();
	private static final Map<String, Long> channels = new HashMap<>();
	private static final Map<String, EloMain> servers = new HashMap<>();

	private static final String CHANNEL_FILE = "data/channels.txt";
	private static final String DATA_PREFIX = "data/";
	private static final String PREFIX = "/chess";

	interface Command {
		String execute(String param, EloMain chess);
	}

	static {
		/*
		 * Add player
		 */
		commands.put("add", (param, chess) -> chess.addPlayer(param).toPrint);
		/*
		 * Play game, either with a mode specified or not.
		 */
		commands.put("play", (param, chess) -> {
			String[] parts = param.split(" ");
			EloResult result = EloResult.INVALID_PARAMS;
			if (parts.length == 1) {
				result = chess.playLichess(parts[0]);
			} else if (parts.length == 3) {
				result = chess.playGame(null, parts[0], parts[1], parts[2]);
			} else if (parts.length == 4) {
				result = chess.playGame(parts[0], parts[1], parts[2], parts[3]);
			}
			return result.toPrint;
		});
		/*
		 * List games, with optional mode and player.
		 */
		commands.put("games", (param, chess) -> {
			String[] parts = param.split(" ");
			EloResult result = EloResult.INVALID_PARAMS;
			StringBuffer data = new StringBuffer();
			if (param.isEmpty()) {
				result = chess.listGames(null, null, data);
			} else if (parts.length == 1) {
				result = chess.listGames(parts[0], null, data);
			} else if (parts.length == 2) {
				if (parts[0].equals("-")) {
					result = chess.listGames(null, parts[1], data);
				} else {
					result = chess.listGames(parts[0], parts[1], data);
				}
			}
			if (result == EloResult.SUCCESS) {
				return data.toString();
			} else {
				return result.toPrint;
			}
		});
		/*
		 * Remove specified game.
		 */
		commands.put("remove", (param, chess) -> {
			EloResult result;
			if (param.length() != 4 && param.length() != 8) {
				result = EloResult.INVALID_NUMBER;
			} else {
				result = chess.deleteGame(param);
			}
			return result.toPrint;
		});

		/*
		 * List standings, with or without mode.
		 */
		commands.put("top", (param, chess) -> {
			StringBuffer data = new StringBuffer();
			EloResult result;
			if (param.isEmpty()) {
				result = chess.listPlayers(null, data);
			} else {
				result = chess.listPlayers(param, data);
			}
			if (result == EloResult.SUCCESS) {
				return data.toString();
			} else {
				return result.toPrint;
			}
		});
		/*
		 * Add a mode, or list modes.
		 */
		commands.put("mode", (param, chess) -> {
			if (param.isEmpty()) {
				StringBuffer data = new StringBuffer();
				EloResult result = chess.listModes(data);
				if (result == EloResult.SUCCESS) {
					return data.toString();
				} else {
					return result.toPrint;
				}
			} else {
				return chess.addMode(param).toPrint;
			}
		});
		commands.put("link", (param,chess) -> {
			String[] parts = param.split(" ");
			if (parts.length == 2) {
				return chess.linkLichess(parts[0],parts[1]).toPrint;
			} else {
				return EloResult.INVALID_PARAMS.toPrint;
			}
		});
		/*
		 * Print help
		 */
		commands.put("help", (event, chess) -> {
			String result;
			result = "Add player: /chess add <name>\n"
					+ "Play game: /chess play [mode] <p1> <p2> <winner>\n"
					+ "    (use 'DRAW' for draw), (mode can be blank, will default to " + chess.getDefault().getName() + "\n"
					+ "Get scoreboard: /chess top [mode]\n"
					+ "    (mode is optional, displays stats for default mode if blank)\n"
					+ "Get games: /chess games [mode] [player] \n"
					+ "	   (mode is optional, omit both entries to get all games)\n"
					+ "    (player is optional;; to get all games for a player use '-' as the mode)\n"
					+ "Remove a game: /chess remove <game>\n"
					+ "Deal with modes: /chess mode [name]\n"
					+ "    (no parameter: list modes || parameter: add mode)\n"
					+ "    (the first mode listed/added is the default mode)\n"
					+ "**LICHESS**\n"
					+ "Link user: /chess link <player> <username>\n"
					+ "Add game: /chess play <URL>";
			return result;
		});
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Must have one parameter with the bot token.");
			return;
		}
		Scanner channelReader;
		try {
			channelReader = new Scanner(new File(CHANNEL_FILE));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		while (channelReader.hasNextLine()) {
			String[] parts = channelReader.nextLine().split(" ");
			channels.put(parts[0], Long.parseLong(parts[1]));
		}
		final DiscordClient client = new DiscordClientBuilder(args[0]).build();
		client.getEventDispatcher().on(ReadyEvent.class)
				.subscribe(event -> {
					User self = event.getSelf();
					System.out.println(String.format("Logged in as %s#%s", self.getUsername(), self.getDiscriminator()));
				});
		client.getEventDispatcher().on(GuildCreateEvent.class)
				.subscribe(event -> {
					String serverID = event.getGuild().getId().asString();
					System.out.println(serverID + " init");
					servers.put(serverID, DataManager.load(DATA_PREFIX + serverID + ".json"));
				});
		client.getEventDispatcher().on(MessageCreateEvent.class)
				.subscribe(event -> {
					final String content = event.getMessage().getContent().orElse("");
					//check channel
					long id = event.getMessage().getChannelId().asLong();
					String guild = event.getMessage().getGuild().block().getId().asString();
					if (!channels.containsKey(guild) || channels.get(guild) != id)
						return;
					//check formatting
					String[] parts = content.split(" ", 3);
					if (parts.length < 2 || !parts[0].equals(PREFIX))
						return;
					//check command
					EloMain chess = servers.get(guild);
					Command cmd = commands.get(parts[1]);
					if (cmd == null)
						return;
					//run command
					String param = parts.length == 3 ? parts[2] : "";
					String result = cmd.execute(param, chess);
					//send message and try to save.
					event.getMessage().getChannel().block().createMessage(result).block();
					try {
						DataManager.save(chess, DATA_PREFIX + guild + ".json");
					} catch (IOException e) {
						e.printStackTrace();
					}
				});
		client.login().block();
	}
}
