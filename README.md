# elochess
EloChess is a discord bot designed to allow for tracking of chess games in a server.

## Repo Setup
1. Clone the repository.
2. Make a folder in the root directory called `data`.
3. In the data folder, make a file called `channels.txt`. In this file, add a line for each server the bot is going to be in, in the format `serverID channelID`, where `channelID` is the channel you want the bot to be used in. You may have to enable developer settings in discord to see IDs.
4. Run `gradlew build` to get dependencies.
5. At this point, you're setup for editing in an IDE. If you want an executable jar, run `gradlew jar`. It will be in `build/libs`.
6. The program requires a command line argument that is the bot token. Go to the discord developer API website to get this. (DO NOT SHARE)

